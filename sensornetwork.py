import math
import sys

# check the max subset
def max_sensors(result_list, li) :
  res_list = []
  for i in range(1, len(result_list)) :
    new_index = [i]
    for j in range(len(result_list)) :
       if j not in new_index and euclidian(li[result_list[i]], li[result_list[j]]) <= d :
            new_index.append(j)
    if len(new_index) > len(res_list) :
       res_list = new_index
  return res_list

#To check the euclidian distance
def euclidian(p1, p2):
    val = math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)
    return val


#checking each sensor
def sensor_check(n, d, li) :
    result_list = []
    for i in range(n) :
        checked_index = [i]
    for j in range(n) :
        if j not in checked_index and euclidian(li[i],li[j]) <= d:
            checked_index.append(j)
    if len(checked_index) > len(result_list) :
        result_list = checked_index
        
    result = max_sensors(result_list, li)
    return result
   
  
# Taking inputs and coordinates of sensors
lst = sys.stdin.readlines()
n, d = map(int,lst[0].split())
sens_coords = [tuple(map(int, lst[i].split())) for i in range(1, n + 1)]

max_subset=sensor_check(n, d, sens_coords)

# Output printing
print('\n')
print(len(max_subset))
for k in max_subset:
   print(max_subset[k] + 1, end = ' ')
